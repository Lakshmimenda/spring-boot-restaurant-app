package com.greatlearning.springbootrestaurantsapp.controller;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.springbootrestaurantsapp.service.ItemsServices;
import com.greatlearning.springbootrestaurantsapp.config.AspectConfiguration;
import com.greatlearning.springbootrestaurantsapp.entity.Items;

@RestController
@RequestMapping("/Restaurants/Items")
public class ItemsController {

	@Autowired
	private ItemsServices itemsServices;
	


	@GetMapping("/checkItem")
	public String checkItemExist(Long id) {
		return itemsServices.checkItemExists(id);
	}

	@GetMapping("/viewItems")
	public List<Items> viewItems() {
		
		return itemsServices.viewItems();
	}

	@PostMapping("/selectItems")
	public List<Items> selectedItems(@RequestBody Collection<Long> ids){
			return itemsServices.selectedItems(ids);
	}
	
	@GetMapping("/totalBill")
	public BigInteger totalBill()
	{
		return itemsServices.totalBill();
	}
}
