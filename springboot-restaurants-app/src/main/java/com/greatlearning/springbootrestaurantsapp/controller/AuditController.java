package com.greatlearning.springbootrestaurantsapp.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.springbootrestaurantsapp.entity.Audit;
import com.greatlearning.springbootrestaurantsapp.service.AuditService;

@RestController
@RequestMapping("/Restaurants/Audit")
public class AuditController {

	@Autowired
	private AuditService auditService;
	
	@GetMapping("/getBillsForToday")
	public List<Audit> getBillsForToday() {
	
		return auditService.getBillForToday();
	}
	
	@GetMapping("/getSalesForTheMonth")
	public List<Audit> getSalesForTheMonth() {
	
		return auditService.getSalesForTheMonth();
	}
}
