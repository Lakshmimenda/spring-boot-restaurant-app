package com.greatlearning.springbootrestaurantsapp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.springbootrestaurantsapp.entity.Audit;
import com.greatlearning.springbootrestaurantsapp.entity.User;
import com.greatlearning.springbootrestaurantsapp.service.UserServices;

@RestController
@RequestMapping("/Restaurants/User")
public class UserController {
	
	
	@Autowired
	private UserServices userServices;

	@GetMapping("/getAllUsers")
	public List<User> getAllUsers() {
		return userServices.getUsers();
	}

	@GetMapping("/getUserById")
	public String getUserById(Long id) {
		return userServices.getUsersbyId(id);
	}

	
	@PostMapping("/registerUser")
	public String registerUser(@Validated @RequestBody User user)
	{
		return userServices.registerUser(user);
	}
	 
	@PutMapping("/updateUser")
	public String updateUser(Long id, @RequestBody  User user)
	{
		return userServices.updateUser(id, user);
	}
	
	
	@DeleteMapping("/deleteUser")
	public String deleteUser(Long id)
	{
		return userServices.deleteUser(id);
	}

	
}