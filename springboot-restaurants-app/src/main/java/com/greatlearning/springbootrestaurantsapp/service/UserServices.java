package com.greatlearning.springbootrestaurantsapp.service;

import java.util.List;
import java.util.Optional;

import com.greatlearning.springbootrestaurantsapp.entity.Audit;
import com.greatlearning.springbootrestaurantsapp.entity.User;

public interface UserServices {
	
	public List<User> getUsers();
	public String getUsersbyId(Long id);
	public String updateUser(Long id,User user);
	public String registerUser(User user);
	public String deleteUser(Long id);

}
