package com.greatlearning.springbootrestaurantsapp.service;

import java.util.Date;
import java.util.List;

import com.greatlearning.springbootrestaurantsapp.entity.Audit;

public interface AuditService {

	public List<Audit> getBillForToday();
	public List<Audit> getSalesForTheMonth();
}
