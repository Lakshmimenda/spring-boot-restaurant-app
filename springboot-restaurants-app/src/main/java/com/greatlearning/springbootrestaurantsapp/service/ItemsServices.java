package com.greatlearning.springbootrestaurantsapp.service;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

import com.greatlearning.springbootrestaurantsapp.entity.Audit;
import com.greatlearning.springbootrestaurantsapp.entity.Items;

public interface ItemsServices {

	public List<Items> viewItems();

	public String checkItemExists(Long id);
	
	public List<Items> selectedItems(Collection<Long> ids);
	
	public BigInteger totalBill();
}
