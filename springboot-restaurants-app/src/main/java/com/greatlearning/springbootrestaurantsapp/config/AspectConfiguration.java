package com.greatlearning.springbootrestaurantsapp.config;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;

import com.greatlearning.springbootrestaurantsapp.entity.Audit;
import com.greatlearning.springbootrestaurantsapp.entity.Items;
import com.greatlearning.springbootrestaurantsapp.repository.AuditRepository;
import com.greatlearning.springbootrestaurantsapp.serviceImpl.ItemsServicesImpl;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
@Aspect
@Data
public class AspectConfiguration {
	
	@Autowired
	private AuditRepository auditRepository;
	
	private List<Items> items;
	
	@Autowired
	private ItemsServicesImpl itemsServicesImpl;
	
	
	@AfterReturning("execution(public * com.greatlearning.springbootrestaurantsapp.serviceImpl.ItemsServicesImpl.selectedItems(..) )")
	public void auditingSelectedItems(JoinPoint joinPoint) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		 String userNameSubString = principal.toString().substring(61);
		 
		 List<String> subList = new ArrayList<String>(Arrays.asList(userNameSubString.split(",")));
		
		 String userName = subList.get(0);
		 List<Items> selectedItems = getItems();
		 {
			 selectedItems.forEach(item->{
			auditRepository.saveAndFlush(Audit.builder().createDate(new Date()).itemName(item.getName()).itemPrice(item.getPrice()).userName(userName).build());
		});
		  }
	}

	
	
	public void setSelectedItems(List<Items> items)
	{
		setItems(items);
		}
	
	
}
