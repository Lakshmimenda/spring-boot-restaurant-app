package com.greatlearning.springbootrestaurantsapp.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class SecuirtyConfigurationWithJdbc extends WebSecurityConfigurerAdapter {

	
	  @Autowired
	    private DataSource dataSource;
	  

	    @Autowired
	    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
	        auth.jdbcAuthentication().passwordEncoder(getPasswordEncoder())
	            .dataSource(dataSource)
	            .usersByUsernameQuery("select user_name, password, enabled from user where user_name=?")
	            .authoritiesByUsernameQuery("select user_name, role from user where user_name=?");
	    }

	    @Override
		protected void configure(HttpSecurity http) throws Exception {
		
			  http.csrf().disable().authorizeRequests()
					 .antMatchers("/Restaurants/Items/**").permitAll()
					 .antMatchers("/registerUser").permitAll()
		                .antMatchers("/Restaurants/User/**").hasAuthority("OWNER")
		                .antMatchers("/Restaurants/Audit/**").hasAuthority("OWNER")
		                .anyRequest().authenticated()
					.and()
					
					  .formLogin().permitAll()
					  .and()
					 .logout().permitAll();
					
		}
	@Bean
	PasswordEncoder getPasswordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}


	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/h2-console/**");

	}
}
