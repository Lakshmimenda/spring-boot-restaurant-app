package com.greatlearning.springbootrestaurantsapp.repository;

import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.greatlearning.springbootrestaurantsapp.entity.Items;

@Repository
public interface ItemsRepository extends JpaRepository<Items, Long> {

}