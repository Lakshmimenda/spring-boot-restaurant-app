package com.greatlearning.springbootrestaurantsapp.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.greatlearning.springbootrestaurantsapp.entity.Audit;

@Repository
public interface AuditRepository extends JpaRepository<Audit, Long> {

	 @Query("select a from Audit a where a.createDate = CURRENT_DATE")
		    List<Audit> getAuditForToday();
	 
	 @Query("select a from Audit a where  MONTH(createDate) = MONTH(CURRENT_DATE)")
	    List<Audit> getAuditForCurrentMonth();
	

}
