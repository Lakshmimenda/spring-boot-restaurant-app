package com.greatlearning.springbootrestaurantsapp.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.greatlearning.springbootrestaurantsapp.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	

}
