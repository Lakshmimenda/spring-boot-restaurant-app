package com.greatlearning.springbootrestaurantsapp.serviceImpl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.greatlearning.springbootrestaurantsapp.config.AspectConfiguration;
import com.greatlearning.springbootrestaurantsapp.entity.Audit;
import com.greatlearning.springbootrestaurantsapp.entity.Items;
import com.greatlearning.springbootrestaurantsapp.repository.ItemsRepository;
import com.greatlearning.springbootrestaurantsapp.service.ItemsServices;

import lombok.Data;

@Service
public class ItemsServicesImpl implements ItemsServices {


	@Autowired
	private ItemsRepository itemsRepository;

	@Autowired
	private AspectConfiguration aspectConfiguration;
	
	@Autowired
	private EntityManager entityManager;
	@Override
	public List<Items> viewItems() {
		return itemsRepository.findAll();
	}

	@Override
	public String checkItemExists(Long id) {
		if(!itemsRepository.findById(id).isPresent()) {
			return "Item Does Not Exists";
			
		}
		else {
			return "Item Details : "+ itemsRepository.getById(id);
		}
	}

	@Override
	public List<Items> selectedItems(Collection<Long> ids) {
		aspectConfiguration.setSelectedItems(itemsRepository.findAllById(ids));

	 return itemsRepository.findAllById(ids);
	}

	@Override
	public BigInteger totalBill() {
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		 String userNameSubString = principal.toString().substring(61);
		 
		 List<String> subList = new ArrayList<String>(Arrays.asList(userNameSubString.split(",")));
		
		 String userName = subList.get(0);
		 
		 String queryStr = "select sum(item_price) from audit where user_name = ?1";
	        try {
	            Query query = entityManager.createNativeQuery(queryStr);
	            query.setParameter(1, userName);

	            return   (BigInteger) query.getSingleResult();
	        } catch (Exception e) {
	            e.printStackTrace();
	            throw e;
	
	}

}
}
