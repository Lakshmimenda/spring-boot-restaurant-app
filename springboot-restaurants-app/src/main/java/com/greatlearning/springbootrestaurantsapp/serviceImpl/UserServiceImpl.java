package com.greatlearning.springbootrestaurantsapp.serviceImpl;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.springbootrestaurantsapp.entity.Audit;
import com.greatlearning.springbootrestaurantsapp.entity.User;
import com.greatlearning.springbootrestaurantsapp.repository.UserRepository;
import com.greatlearning.springbootrestaurantsapp.service.UserServices;

@Service
public class UserServiceImpl implements UserServices {

	
	
	@Autowired
	private UserRepository userRepository;
	

	@Autowired
	private EntityManager entityManager;
	
	@Override
	public List<User> getUsers() {
		return userRepository.findAll();
	}

	@Override
	public String getUsersbyId(Long id) {
		if(!userRepository.findById(id).isPresent()) {
			return "User Does Not Exists";
			
		}
		else {
			return "User Details : "+ userRepository.getById(id);
		}
	}

	@Override
	public String updateUser(Long id, User user) {
		if(!userRepository.findById(id).isPresent()) {
			return "User Does Not Exists";
		}
		else {
			 userRepository.deleteById(id);
			 userRepository.saveAndFlush(user);
			 return "User Updated : "+ userRepository.getById(user.getId());
		}
	}

	@Override
	public String registerUser(User user) {
		if(!userRepository.findById(user.getId()).isPresent()) {
		
			 userRepository.save(user);
			 return "User Registered : "+ userRepository.getById(user.getId());
			
		}
		else {
			return "User Already Exists";
		}
	}

	@Override
	public String deleteUser(Long id) {
		if(!userRepository.findById(id).isPresent()) {
			return "User Does Not Exists";
		}
		else {
			 userRepository.deleteById(id);
			 return "User Deleted";
		}
	}

	


}
