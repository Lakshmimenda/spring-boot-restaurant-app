package com.greatlearning.springbootrestaurantsapp.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.springbootrestaurantsapp.entity.Audit;
import com.greatlearning.springbootrestaurantsapp.repository.AuditRepository;
import com.greatlearning.springbootrestaurantsapp.service.AuditService;

@Service
public class AuditServiceImpl implements AuditService {

	@Autowired
	private AuditRepository auditRepository;
	
	@Override
	public List<Audit> getBillForToday() {
		return auditRepository.getAuditForToday();
	}

	@Override
	public List<Audit> getSalesForTheMonth() {
		return auditRepository.getAuditForCurrentMonth();
	}



}
